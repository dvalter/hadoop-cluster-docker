#!/bin/bash

# the default node number is 3
N=${1:-3}

docker network ls | grep hadoop >/dev/null || docker network create --driver=bridge hadoop 
# start hadoop master container
docker rm -f hadoop-master &> /dev/null
echo "start hadoop-master container..."
docker run -v "$(realpath ..):$(realpath ..)" --workdir "$(realpath ..)" -itd \
                --net=hadoop \
                -p 50070:50070 \
                -p 8088:8088 \
                --name hadoop-master \
                --hostname hadoop-master \
                dvalter/hadoop:1.0 &


# start hadoop slave container
i=1
while [ $i -lt $N ]
do
	docker rm -f hadoop-slave$i &> /dev/null
	echo "start hadoop-slave$i container..."
	docker run -itd \
	                --net=hadoop \
	                --name hadoop-slave$i \
                    -p "${i}8088:${i}8088" \
	                --hostname hadoop-slave${i} \
	                dvalter/hadoop:1.0 &
	i=$(( $i + 1 ))
done 

# start hadoop in the master container
docker exec  -it  hadoop-master sh -c 'nohup /root/start-hadoop.sh'

# switch to the project workdir
docker exec  -it  hadoop-master sh -c "cd '$(realpath ..)' && bash"
