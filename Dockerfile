FROM centos:7.8.2003

WORKDIR /root

# install openssh-server, openjdk and wget
RUN yum install -y openssh-server java-1.8.0-openjdk-devel.x86_64  wget

# install hadoop 2.8.5
RUN wget -nv https://downloads.apache.org/hadoop/common/hadoop-2.8.5/hadoop-2.8.5.tar.gz && \
    tar -xzvf hadoop-2.8.5.tar.gz && \
    mv hadoop-2.8.5 /usr/local/hadoop && \
    rm hadoop-2.8.5.tar.gz

# set environment variable
ENV JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk 
ENV HADOOP_HOME=/usr/local/hadoop 
ENV PATH=$PATH:/usr/local/hadoop/bin:/usr/local/hadoop/sbin 

# ssh without key
RUN ssh-keygen -t rsa -f ~/.ssh/id_rsa -P '' && \
    cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

RUN mkdir -p ~/hdfs/namenode && \ 
    mkdir -p ~/hdfs/datanode && \
    mkdir $HADOOP_HOME/logs

RUN yum install -y openssh-clients which maven

COPY config/* /tmp/

RUN mv /tmp/ssh_config ~/.ssh/config && \
    mv /tmp/hadoop-env.sh /usr/local/hadoop/etc/hadoop/hadoop-env.sh && \
    mv /tmp/hdfs-site.xml $HADOOP_HOME/etc/hadoop/hdfs-site.xml && \ 
    mv /tmp/core-site.xml $HADOOP_HOME/etc/hadoop/core-site.xml && \
    mv /tmp/mapred-site.xml $HADOOP_HOME/etc/hadoop/mapred-site.xml && \
    mv /tmp/yarn-site.xml $HADOOP_HOME/etc/hadoop/yarn-site.xml && \
    mv /tmp/slaves $HADOOP_HOME/etc/hadoop/slaves && \
    mv /tmp/start-hadoop.sh ~/start-hadoop.sh && \
    mv /tmp/run-wordcount.sh ~/run-wordcount.sh

RUN chmod +x ~/start-hadoop.sh && \
    chmod +x ~/run-wordcount.sh && \
    chmod +x $HADOOP_HOME/sbin/start-dfs.sh && \
    chmod +x $HADOOP_HOME/sbin/start-yarn.sh 

RUN chmod -R go-rwx /root/.ssh
RUn /usr/sbin/sshd-keygen

# format namenode
RUN /usr/local/hadoop/bin/hdfs namenode -format

CMD [ "sh", "-c", "/usr/sbin/sshd -D; bash"]

