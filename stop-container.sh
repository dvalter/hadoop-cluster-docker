#!/bin/bash

# the default node number is 3
N=${1:-3}

docker rm -f hadoop-master &> /dev/null
echo "stop hadoop-master container..."
docker kill hadoop-master

i=1
while [ $i -lt $N ]
do
	echo "stop hadoop-slave$i container..."
    docker kill hadoop-slave$i
    i=$(( $i + 1 ))
done 
