#!/bin/bash

# test the hadoop cluster by running wordcount

# create input files 
mkdir wc-input
echo "Hello Docker" >wc-input/file2.txt
echo "Hello Hadoop" >wc-input/file1.txt

# create input directory on HDFS
hadoop fs -mkdir -p wc-input

# put input files to HDFS
hdfs dfs -put ./wc-input/* wc-input

# run wordcount 
hadoop jar $HADOOP_HOME/share/hadoop/mapreduce/sources/hadoop-mapreduce-examples-2.8.5-sources.jar org.apache.hadoop.examples.WordCount wc-input wc-output

# print the input files
echo -e "\ninput file1.txt:"
hdfs dfs -cat wc-input/file1.txt

echo -e "\ninput file2.txt:"
hdfs dfs -cat wc-input/file2.txt

# print the output of wordcount
echo -e "\nwordcount output:"
hdfs dfs -cat wc-output/part-r-00000

